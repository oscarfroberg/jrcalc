Junk Rig Calculator
===================

Demo at [http://jrcalc.oscarfroberg.com](http://jrcalc.oscarfroberg.com)

The purpose of this is to collect all (or most of) the calculations used while designing a junk rig in one place.
