function isNumber(n)
{
        return !isNaN(parseFloat(n)) && isFinite(n) && (n != 0);
}

function sparrounder(spar_mass)
{
        var decimals = 0;
	if (spar_mass <= 1) decimals = 3;
	else if (spar_mass <= 10) decimals = 2;
	else if (spar_mass <= 50) decimals = 1;
	else decimals = 0;
        spar_mass = spar_mass.toFixed(decimals);
        return spar_mass;
}

function sadispratiorounder(ratio)
{
        var decimals = 0;
        if (ratio <= 1) decimals = 3;
        else if (ratio <= 15) decimals = 2;
        else if (ratio <= 30) decimals = 1;
        else decimals = 0;
        ratio = ratio.toFixed(decimals);
        return ratio;
}

function settoempty(fields)
{
        for (var i=0; i<fields.length; i++) {
                $(fields[i]).val("");
        }

}

function clearfields(calc)
{
        switch (calc) {
        case "spar":
                var fields = ["#spar_length", "#spar_diameter", "#spar_topdiameter", "#spar_thickness", "#spar_mass", "#spar_bs"];
                settoempty(fields);
                break;
        case "hull":
                var fields = ["#hull_sa", "#hull_disp", "#hull_lwl", "#hull_beam", "#hull_sadisp_ratio", "#hull_displwl_ratio", "#hull_hullspeed", "#hull_righting"];
                settoempty(fields);
                break;
        case "sail":
                var fields = ["#sail_sa", "#sail_ft_yard", "#sail_ft_b1", "#sail_ft_b2", "#sail_ft_b3", "#sail_ft_b4", "#sail_ft_b5", "#sail_ft_b6", "#sail_ft_b7", "#sail_ft_boom", "#sail_ft_mastlap", "#sail_ft_mastloa"];
                settoempty(fields);
                break;
        case "angle":
                var fields = ["#angle_length", "#angle_angle", "#angle_distance"];
                settoempty(fields);
                break;
        }
}

function enableinput(field)
{
        $(field).prop("disabled", false);
}

function disableinput(field)
{
        $(field).prop("disabled", true);
}

function changefield(type, action)
{
        if (type == "topdiameter" && action == "enable") {
                enableinput("#spartop input");
        }
        if (type == "topdiameter" && action == "disable") {
                disableinput("#spartop input");
        }
        if (type == "thickness" && action == "enable") {
                enableinput("#sparthickness input");
        }
        if (type == "thickness" && action == "disable") {
                disableinput("#sparthickness input");
        }
        if (type == "b7" && action == "enable") {
                enableinput("#sparthickness input");
        }
        if (type == "b7" && action == "disable") {
                disableinput("#sparthickness input");
        }
}

function getvar(variable)
{
        var datatype = "#" + variable;
        variable = $(datatype).val();
        variable = variable.replace(",",".");
        return variable;
}

function sparcalc()
{
        /* assign variables */
        var spar_length = getvar("spar_length");
        var spar_diameter = getvar("spar_diameter");
        var spar_topdiameter = getvar("spar_topdiameter");
        var spar_thickness = getvar("spar_thickness");
        var spar_radius = spar_diameter / 2;
        var spar_topradius = spar_topdiameter / 2;
        var spar_innerradius = spar_radius - spar_thickness;
        var spar_material = $("#spar_material").val();
        spar_material = spar_material.split(",");
        var spar_density = spar_material[0];
        var spar_mpa = spar_material[1];
        var spar_kpm2 = spar_mpa * 101972.5;

        var spartype = $("#spar_type").val();
        var spartaper = $("#spar_taper").val();

        /* calculations for solid spar */
        if (spartype == "solid") {
                if (spartaper == "false") {
                        changefield("topdiameter", "disable");
                        changefield("thickness", "disable");
                        /* calculate mass */
		        var spar_mass = spar_radius * spar_radius * Math.PI * spar_length / 1000 * spar_density;
                        /* calculate breaking strength */
                        var spar_bs = spar_kpm2 * ((Math.PI/32) * (Math.pow(spar_diameter/1000, 4)/(spar_diameter/1000)));
                }
                if (spartaper == "true") {
                        changefield("topdiameter", "enable");
                        changefield("thickness", "disable")
                        /* calculate mass */
		        var spar_mass = Math.PI * spar_length * (spar_radius * spar_radius + spar_topradius * spar_topradius + spar_radius * spar_topradius) / 3 / 1000 * spar_density;
                        /* calculate breaking strength
                        * THIS MIGHT BE THE WAY TO DO IT? var avg_dia = (spar_diameter/1000 + spar_topdiameter/1000) / 2;
                        * instead we're accounting for just some taper at partner level (where it counts): */
                        var hack_diameter = spar_diameter * 0.95;
                        var spar_bs = spar_kpm2 * ((Math.PI/32) * (Math.pow(hack_diameter/1000, 4)/(hack_diameter/1000)));
                }
        }

        /* calculations for hollow spar */
        if (spartype == "hollow") {
                if (spartaper == "false") {
                        changefield("topdiameter", "disable");
                        changefield("thickness", "enable")
                        /* calculate mass */
		        var spar_basearea = spar_radius * spar_radius * Math.PI - spar_innerradius * spar_innerradius * Math.PI;
 		        var spar_mass = spar_basearea * spar_length / 1000 * spar_density;
                        /* calculate breaking strength */
                        var spar_innerdiameter = spar_diameter - spar_thickness*2;
                        var spar_bs = spar_kpm2 * ((Math.PI/32) * ((Math.pow(spar_diameter/1000, 4)-Math.pow(spar_innerdiameter/1000, 4))/(spar_diameter/1000)));
                }
                if (spartaper == "true") {
                        changefield("topdiameter","enable");
                        changefield("thickness", "enable");
		        var spar_solidmass = Math.PI * spar_length * (spar_radius * spar_radius + spar_topradius * spar_topradius + spar_radius * spar_topradius) / 3 / 1000;
		        /* calculate "inner tapered tube" to substract from solid mass to get hollow mass */
		        var spar_sub_radius = spar_radius - spar_thickness;
		        var spar_sub_topradius = spar_topradius - spar_thickness;
		        var spar_substractmass = Math.PI * spar_length * (spar_sub_radius * spar_sub_radius + spar_sub_topradius * spar_sub_topradius + spar_sub_radius * spar_sub_topradius) / 3 / 1000;
		        var spar_mass = spar_solidmass - spar_substractmass;
		        spar_mass = spar_mass * spar_density;
                        /* calculate breaking strength */
                        var hack_diameter = spar_diameter * 0.95;
                        var hack_innerdiameter = hack_diameter - spar_thickness*2;
                        var spar_bs = spar_kpm2 * ((Math.PI/32) * ((Math.pow(hack_diameter/1000, 4)-Math.pow(hack_innerdiameter/1000, 4))/(hack_diameter/1000)));
                }
        }
    
        /* round to appropriate decimals */
        spar_mass = sparrounder(spar_mass);
        /* change value of mass textbox */
        if (spar_mass != 0) $("#spar_mass").val(spar_mass);
        spar_bs = spar_bs.toFixed(0);
        if (isNumber(spar_bs)) $("#spar_bs").val(spar_bs);
}

function anglecalc()
{
        /* get variables from form */
        var angle_length = getvar("angle_length");
        var angle_angle = getvar("angle_angle");
        var angle_distance = getvar("angle_distance");

        var angle_calctype = $("#angle_calctype").val();

        /* choose which method to calculate by */

        switch (angle_calctype) {

        /* if we want to solve for angle */
        case "angle":
                var angleresult = Math.atan((0.5*angle_distance)/angle_length);
                angleresult = angleresult*2*57.2957795130823;
                angleresult = angleresult.toFixed(2);
                if (isNumber(angleresult)) $("#angle_angle").val(angleresult);
                break;

        /* if we want to solve for distance */
        case "distance":
                angle_angle = (angle_angle / (57.2957795130823) * 0.5);
                var angleresult = (Math.tan(angle_angle))*angle_length*2;
                angleresult = angleresult.toFixed(2);
                if (isNumber(angleresult)) $("#angle_distance").val(angleresult);
                break;

        /* if we want to solve for length */
        case "length":
                angle_angle = (angle_angle / (57.2957795130823) *0.5);
                angleresult = angle_distance * 0.5 / (Math.tan(angle_angle));
                angleresult = angleresult.toFixed(2);
                if (isNumber(angleresult)) $("#angle_length").val(angleresult);
                break;
        }
}

function hullcalc()
{
        var hull_sa = getvar("hull_sa");
        var hull_disp = getvar("hull_disp");
        var hull_lwl = getvar("hull_lwl");
        var hull_beam = getvar("hull_beam");
        var hull_tons = hull_disp / 1000;
        /* calculate sail area to displacement ratio */
        var hull_ratio = hull_sa / Math.pow(hull_tons, 0.6667);
        /* round the result appropriately */
        hull_ratio = sadispratiorounder(hull_ratio);
        /* insert it to form */
        if (isNumber(hull_ratio)) $("#hull_sadisp_ratio").val(hull_ratio);
    
        /* calculate displacement to waterline */
        var hull_lwl_ft = hull_lwl * 3.2808;
        /* dividing displacement (in tons) by 1/100th of the LWL in feet cubed */
        var hull_displwl_ratio = hull_tons / Math.pow(0.01*hull_lwl_ft, 3);
        hull_displwl_ratio = hull_displwl_ratio.toFixed(0);
        if (isNumber(hull_displwl_ratio)) $("#hull_displwl_ratio").val(hull_displwl_ratio);

        /* calculate hull speed */
        var hull_hullspeed = 1.34 * Math.sqrt(hull_lwl_ft);
        /* round to two decimals */
        hull_hullspeed = hull_hullspeed.toFixed(2);
        if (isNumber(hull_hullspeed)) $("#hull_hullspeed").val(hull_hullspeed);

        /* calculate righting moment */
        var hull_righting = hull_disp * hull_beam / 4.5;
        hull_righting = hull_righting.toFixed(0);
        if (isNumber(hull_righting)) $("#hull_righting").val(hull_righting);
}

function sailcalc()
{
        var sail_area = getvar("sail_sa");
        switch ($("#sail_type").val()) {
        case "fantail":
                /* calcs based on david tyler's fantail planform, mast lap changed from 1.562 to 1.497 as per his recommendations */
                var areasq = Math.sqrt(sail_area);
                var yard = areasq*0.6821;
                var b1 = areasq*0.8422;
                var b2 = areasq*0.8893;
                var b3 = areasq*0.9032;
                var b4 = areasq*0.9032;
                var b5 = areasq*0.9032;
                var b6 = areasq*0.9032;
                var b7 = areasq*0.8893;
                var boom = areasq*0.8573;
                var mastlap = areasq*1.497;
                var mastloa = mastlap*1.1;
                $("#sail_ft_yard").val(yard.toFixed(2));
                $("#sail_ft_b1").val(b1.toFixed(2));
                $("#sail_ft_b2").val(b2.toFixed(2));
                $("#sail_ft_b3").val(b3.toFixed(2));
                $("#sail_ft_b4").val(b4.toFixed(2));
                $("#sail_ft_b5").val(b5.toFixed(2));
                $("#sail_ft_b6").val(b6.toFixed(2));
                $("#sail_ft_b7").val(b7.toFixed(2));
                changefield("b7", "enable");
                $("#sail_ft_boom").val(boom.toFixed(2));
                $("#sail_ft_mastlap").val(mastlap.toFixed(2));
                $("#sail_ft_mastloa").val(mastloa.toFixed(2));
                break;
        case "johanna":
                /* calcs based on arne's 48 m2 sail with 5.8m battens, 10.6m mast */
                var areasq = Math.sqrt(sail_area);
                var yard = areasq*0.8372;
                var b1 = areasq*0.8372;
                var b2 = areasq*0.8372;
                var b3 = areasq*0.8372;
                var b4 = areasq*0.8372;
                var b5 = areasq*0.8372;
                var b6 = areasq*0.8372;
                var boom = areasq*0.8372;
                var mastloa = areasq*1.53;
                var mastlap = mastloa/1.1;
                $("#sail_ft_yard").val(yard.toFixed(2));
                $("#sail_ft_b1").val(b1.toFixed(2));
                $("#sail_ft_b2").val(b2.toFixed(2));
                $("#sail_ft_b3").val(b3.toFixed(2));
                $("#sail_ft_b4").val(b4.toFixed(2));
                $("#sail_ft_b5").val(b5.toFixed(2));
                $("#sail_ft_b6").val(b6.toFixed(2));
                changefield("b7", "disable");
                $("#sail_ft_boom").val(boom.toFixed(2));
                $("#sail_ft_mastlap").val(mastlap.toFixed(2));
                $("#sail_ft_mastloa").val(mastloa.toFixed(2));
                break;
        }
}

$("#spar").bind('change keyup', function(event) {
        sparcalc()
});

$("#angle").bind('change keyup', function(event) {
        anglecalc()
});

$("#hull").bind('change keyup', function(event) {
        hullcalc()
});

$("#sail").bind('change keyup', function(event) {
        sailcalc()
});


